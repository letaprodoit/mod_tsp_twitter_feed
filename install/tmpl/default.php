<?php 
/*
 * TSP Twitter Feed Joomla! module
 *
 * @package		TSP Twitter Feed Joomla! module
 * @filename	default.php
 * @version		1.0.0
 * @author		Sharron Denice, The Software People, LLC on 2013/02/09
 * @copyright	Copyright © 2013 The Software People, LLC (www.thesoftwarepeople.com). All rights reserved
 * @license		APACHE v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 * @brief		default template
 * 
 */

defined('_JEXEC') or die('Restricted access'); // no direct access 

?>

<h2 class='tsp_tf_title'><div class='icon_twitter'><span class='title'><?php echo $title;?></span></div></h1>
<?php if ($carousel_on) : ?>  
<script type="text/javascript">  
	jQuery(document).ready(function() {
		jQuery(".tsp_tf_carousel").jCarouselLite({  
	        vertical: 	true,  
	        visible: 	<?php echo $visible_tweets;?>,  
	        auto:	 	<?php echo $auto;?>,  
	        speed: 		<?php echo $interval;?>,  
	    });  
	});  
</script>
<div class="tsp_tf_carousel" style="width: <?php echo $block_width;?>px; height: <?php echo ($visible_tweets * 119) + ($visible_tweets * 15);?>px;">
	<ul>
		<?php foreach ($feed as $entry => $tweet) : ?>
			<li class="<?php echo $tweet['type'];?>" style="width: <?php echo $block_width;?>px;"><?php echo $tweet['text'];?></li>
		<?php endforeach; ?>
	</ul>	
</div>
<?php else : ?>
<?php echo $feed; ?>
<?php endif; ?>
