<?php
/*
 * TSP Twitter Feed Joomla! module
 *
 * @package		TSP Twitter Feed Joomla! module
 * @filename	helper.php
 * @version		1.0.0
 * @author		Sharron Denice, The Software People, LLC on 2013/02/09
 * @copyright	Copyright © 2013 The Software People, LLC (www.thesoftwarepeople.com). All rights reserved
 * @license		APACHE v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 * @brief		helper class
 * 
 */

defined('_JEXEC') or die('Direct Access to this location is not allowed.');
 
class ModTSPTwitterFeedHelper
{
    /***********
     *
     * Returns twitter widget
     *
     ***********/
    public static function getWidget($params)
    {
		$type = $params->get('type');
		
		$javascript = "";
		
		$javascript .= '<script src="http://widgets.twimg.com/j/2/widget.js"></script>';
		$javascript .= '<script>';
		$javascript .= 'new TWTR.Widget({';
		$javascript .= '  version: 2,';
		$javascript .= '  type: "'.$type.'",';
		$javascript .= '  interval: '.$params->get('interval').',';
		
		if (in_array($type, array('profile','faves','list')))
		{
			$javascript .= '    rpp: '.$params->get('num_tweets').',';
		}//endif
		
		if (in_array($type, array('search')))
		{
			$javascript .= '  search: "'.$params->get('search').'",';
		}//endif
		
		// Title not used here but in the tmpl/default.php
		// Subject not used at all
		if (in_array($type, array('search','faves','list')))
		{
			$javascript .= '  title: "",';
			$javascript .= '  subject: "",';
		}//endif
		
		$javascript .= '  width: "'.$params->get('block_width').'",';
		$javascript .= '  height: "'.$params->get('block_height').'",';
		$javascript .= '  theme: {';
		$javascript .= '    shell: {';
		$javascript .= '      background: "transparent",';
		$javascript .= '      color: ""'; /* Color not used since shell is hidden */
		$javascript .= '    },';
		$javascript .= '    tweets: {';
		$javascript .= '      background: "",'; /* Background set in stylesheet */
		$javascript .= '      color: "'.$params->get('tweet_text').'",';
		$javascript .= '      links: "'.$params->get('tweet_link').'"';
		$javascript .= '    }';
		$javascript .= '  },';
		$javascript .= '  features: {';
		$javascript .= '    scrollbar: '.$params->get('scrollbar').',';
		$javascript .= '    loop: '.$params->get('loop').',';
		$javascript .= '    live: '.$params->get('live').',';
		$javascript .= '    hashtags: '.$params->get('hashtags').',';
		$javascript .= '    timestamp: '.$params->get('timestamp').',';
		$javascript .= '    avatars: '.$params->get('avatars').',';
		
		if (in_array($type, array('search')))
		{
			$javascript .= '    toptweets: '.$params->get('toptweets').',';
		}//endif
		
		$javascript .= '    behavior: "'.$params->get('behavior').'"';
		$javascript .= '  }';
		
		if (in_array($type, array('profile','faves')))
		{
			$javascript .= '}).render().setUser("'.$params->get('username').'").start();';
		}
		elseif (in_array($type, array('list')))
		{
			$javascript .= '}).render().setList("'.$params->get('username').',"'.$params->get('list').'").start();';
		} else {
			$javascript .= '}).render().start();';
		}//endif
		
		$javascript .= '</script>';
		
		return $javascript;
    }//end getWidget
    
    /***********
     *
     * Returns twitter carousel feed
     *
     ***********/
    public static function getCarousel($params)
    {
    	$max_tweets = $params->get('max_tweets');
    	$feed_url = "http://twitter.com/statuses/user_timeline.xml?screen_name={$params->get('username')}";
    	
        $tweets = array();
		$result_xml = "";

        // Use curl to get the xml feed
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $feed_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		$result_xml = curl_exec($ch);
        curl_close ($ch);

		// If the feed was returned process
		if ($result_xml)
		{
			$xmlDoc = new DOMDocument();
			$xmlDoc->loadXML($result_xml);

			// Convert the XML to a hash
			$status = ModSimpleTwitterFeed::xmlToHash('status',$xmlDoc);
			
			// if we are returning all tweets or if $max_tweets greater
			// than the amount of tweets return only return the tweets generated
			if (!$max_tweets || $max_tweets > count($status))
			{
				$max_tweets = count($status);
			}//endif

			// loop through the tweets
			for ($i = 0; $i < $max_tweets; $i++)
			{
				$type = "twitter";
								
				// add a target blank to all urls
				if (preg_match("/http\:\/\/(.*)/", $status[$i]['text'],$matches))
				{
					$url = $matches[0];
					$txt_url = $matches[1];

					$status[$i]['text'] = str_replace("$url", "<a href='$url' target='_blank'>$url</a>", $status[$i]['text']);
				}//endif

				// set the type of tweet
				if (preg_match("/facebook/", $status[$i]['source']))
				{
					$type = "facebook";
				}//endif
				
				// add urls and target blank to at replies
				if (preg_match("/\@([a-zA-Z0-9_]*)/", $status[$i]['text'],$matches))
				{
					$at_reply = $matches[0];
					$screen_name = $matches[1];
					
					$status[$i]['text'] = str_replace("$at_reply", "<a href='http://twitter.com/#!/$screen_name' data-screen-name='$screen_name' target='_blank'>$at_reply</a>", $status[$i]['text']);
				}//endif

				// add urls and target blank to trends
				if (preg_match("/\#([a-zA-Z0-9_]*)/", $status[$i]['text'],$matches))
				{
					$trend = $matches[0];
					$search = $matches[1];
					
					$status[$i]['text'] = str_replace("$search", "<a href='http://twitter.com/#!/search?q=%23$search' title='$trend' target='_blank'>$trend</a>", $status[$i]['text']);
				}//endif

				$timestamp = strtotime($status[$i]['created_at']);
				$status[$i]['created_at'] = date('M d, Y h:i:s A',$timestamp);
	
				$a_tweet_text = "<div class='wrapper'>\n\t<div class='text'>";
				$a_tweet_text .= "\t\t<span class='text'><span class='title'>Date</span>: <span class='date'>".$status[$i]['created_at']."</span>";
				$a_tweet_text .= "\t\t<br>".$status[$i]['text']."</span>";
				$a_tweet_text .= "\n\t</div>\n</div>";
				
				$a_tweet = array('type' => $type, 'text' => $a_tweet_text);
				
				// Place the tweeet into the stack
				array_push($tweets, $a_tweet);
			}//end for
		}//end if

        return ($tweets);
    } //end getCarousel
    
    /***********
     *
     * Returns a hash array of an xml node
     *
     ***********/
    public static function xmlToHash($nodeName, $xmlDoc)
    {
        $hash = array();
		$debug = false;

        $xmlList = $xmlDoc->getElementsByTagName($nodeName); //DOMNodeList

        for ($i = 0; $i < $xmlList->length; $i++)
        {
            $arr = array();

            $xmlNode = $xmlList->item($i); //DOMNode

            if ($xmlNode->hasChildNodes())
            {
                $xmlChild = $xmlNode->firstChild; //DOMElement

                while ($xmlChild)
                {
                    if ($xmlChild->localName)
                    {
                        $name = $xmlChild->localName;
						$val = html_entity_decode($xmlChild->nodeValue, ENT_QUOTES);

                        $arr[$name] = $val;
                        if ($debug){echo "\$arr[$name] = ($val);<br>\n";}
                    }//endif
                    $xmlChild = $xmlChild->nextSibling;
                }//endwhile
            }//endif

            array_push($hash, $arr);
        }//endfor

        return $hash;
    }//end xmlToHash
    
} //end ModTSPTwitterFeedHelper
?>