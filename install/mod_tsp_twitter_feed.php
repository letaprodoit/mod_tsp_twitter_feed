<?php
/*
 * TSP Twitter Feed Joomla! module
 *
 * @package		TSP Twitter Feed Joomla! module
 * @filename	mod_tsp_twitter_feed.php
 * @version		1.0.0
 * @author		Sharron Denice, The Software People, LLC on 2013/02/09
 * @copyright	Copyright © 2013 The Software People, LLC (www.thesoftwarepeople.com). All rights reserved
 * @license		APACHE v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 * @brief		module loader
 * 
 */

//no direct access
defined('_JEXEC') or die('Direct Access to this location is not allowed.');
 
// Step #1: include the helper file
require_once __DIR__ . '/helper.php';

// Step #2: get user settings
$carousel_on = $params->get('carousel_on') == 'true' ? 1 : 0;

// These settings are used by both feed and carousel in html
$username = $params->get('username');
$title = $params->get('title');
$block_width = $params->get('block_width');
$num_tweets = $params->get('num_tweets');
$visible_tweets = $params->get('visible_tweets');
$auto = $params->get('auto');
$interval = $params->get('interval');
 
// Step #3: get the current url
if ((bool)$carousel_on)
{
	$feed = ModTSPTwitterFeedHelper::getCarousel($params);
} else {
	$feed = ModTSPTwitterFeedHelper::getWidget($params);
}//endif
 
$document = JFactory::getDocument();
$document->addStyleSheet('modules/mod_tsp_twitter_feed/tmpl/css/mod_tsp_twitter_feed.css');

if ((bool)$carousel_on)
{
	$document->addScript('modules/mod_tsp_twitter_feed/tmpl/js/jquery.min.js');
	$document->addScript('modules/mod_tsp_twitter_feed/tmpl/js/jcarousellite_1.0.1c4.js');
}//endif

// Step #4: include the template for display
require(JModuleHelper::getLayoutPath('mod_tsp_twitter_feed'));
?>