Twitter Feed (Joomla! Module) 1.0
-------
Twitter Feed (Joomla! Module) software allows users to add a scrolling ticker of Twitter 
favorites, reposts, tweets and searches to their website.

For up-to-date installation and setup notes, visit the FAQ:
http://lab.thesoftwarepeople.com/tracker/wiki/joomla-tf-MainPage


*GENERAL INSTALLATION NOTES*

- Download from repository
- Open Joomla Administration Control Panel
- Open Extension Manager (Install) link
- Browse for the downloaded zip file in the Upload Package File section
- Click Upload & Install

*USING THE MODULE*

- Open Joomla Administration Control Panel
- Open Module Manager
- Find the "The Software People - Twitter Feed" listed and unpublished
-- If not listed click on "New" and click on "The Software People - Twitter Feed"
- Complete the basic details and decide on which pages the Twitter Feed will be displayed
- Update the module preferences mouse over tooltips for instruction

*REPORTING ISSUES*

Thank you for downloading the Twitter Feed (Joomla! Module) 1.0
If you find any issues, please report them in the issue tracker on our website:
http://lab.thesoftwarepeople.com/tracker/joomla-tf